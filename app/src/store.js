import { ref } from 'vue';
import { defineStore } from 'pinia';

export const useStore = defineStore('store', () => {
  const from = ref({
    server: 'localhost',
    port: 3143,
    username: 'from@example.org',
    password: 'password',
    tls: false,
  });

  const to = ref({
    server: 'localhost',
    port: 31432,
    username: 'to@example.org',
    password: 'password',
    tls: false,
  });

  return {
    from,
    to,
  };
});
