import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { createRouter } from '~/router';
import App from '~/App';

const app = createApp(App);

app.provide('electron', require('electron'));

app.use(createRouter());
app.use(createPinia());

app.mount('#app');
