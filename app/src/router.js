import { createRouter as createVueRouter, createWebHashHistory } from 'vue-router';
import Step1 from '~/pages/Steps/1-From';
import Step2 from '~/pages/Steps/2-To';
import Step3 from '~/pages/Steps/3-Migrate';

export function createRouter() {
  return createVueRouter({
    history: createWebHashHistory(),
    routes: [
      {
        path: '/',
        redirect: '/steps/1',
      },
      {
        path: '/steps/1',
        component: Step1,
      },
      {
        path: '/steps/2',
        component: Step2,
      },
      {
        path: '/steps/3',
        component: Step3,
      },
    ],
  });
};
