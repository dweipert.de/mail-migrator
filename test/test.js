const { ImapFlow } = require('imapflow');

const fromClient = new ImapFlow({
  host: 'localhost',
  port: 3143,
  auth: {
    user: 'from@example.org',
    pass: 'password',
  },
});

const toClient = new ImapFlow({
  host: 'localhost',
  port: 31432,
  auth: {
    user: 'to@example.org',
    pass: 'password',
  },
});

(async () => {
  await fromClient.connect();
  await toClient.connect();

  const fromMailboxes = [
    'From Test Parent.From Test Child',
    'From Test Parent.From Test Child 2.From Test Child Inner',
    '10000 Mails',
  ];
  const toMailboxes = [
    'To Test Parent.To Test Child',
    'To Test Parent.To Test Child 2.To Test Child Inner',
  ];
  for (const mailbox of fromMailboxes) {
    try {
      await fromClient.mailboxDelete(mailbox);
    } catch (e) {}
    try {
      await toClient.mailboxDelete(mailbox);
    } catch (e) {}

    try {
      await fromClient.mailboxCreate(mailbox);
    } catch (e) {}
  }
  for (const mailbox of toMailboxes) {
    try {
      await toClient.mailboxDelete(mailbox);
    } catch (e) {}

    try {
      await toClient.mailboxCreate(mailbox);
    } catch (e) {}
  }

  fromClient.append('INBOX',
    'From: test@example.org\r\n' +
    'To: from@example.org\r\n' +
    'Subject: FROM INBOX MESSAGE SUBJECT\r\n' +
    '\r\n' +
    'FROM INBOX MESSAGE BODY', [], new Date());

  fromClient.append('From Test Parent.From Test Child',
    'From: test@example.org\r\n' +
    'To: from@example.org\r\n' +
    'Subject: FROM TEST CHILD MESSAGE FLAGGED SUBJECT\r\n' +
    '\r\n' +
    'FROM TEST CHILD MESSAGE FLAGGED BODY', ['\\FLAGGED'], new Date());

  fromClient.append('From Test Parent.From Test Child',
    'From: test@example.org\r\n' +
    'To: from@example.org\r\n' +
    'Subject: FROM TEST CHILD MESSAGE DRAFT SUBJECT\r\n' +
    '\r\n' +
    'FROM TEST CHILD MESSAGE DRAFT BODY', ['\\DRAFT'], new Date());

  fromClient.append('From Test Parent.From Test Child 2.From Test Child Inner',
    'From: test@example.org\r\n' +
    'To: from@example.org\r\n' +
    'Subject: FROM TEST CHILD 2 INNER MESSAGE SEEN SUBJECT\r\n' +
    '\r\n' +
    'FROM TEST CHILD 2 INNER MESSAGE SEEN BODY', ['\\SEEN'], new Date());

  for (let i = 0; i < Number(process.env.COUNT || 10000); i++) {
    const flag = ['\\SEEN', '', '\\FLAGGED'][Math.floor(Math.random() * 3)];
    const now = new Date();

    fromClient.append('10000 Mails',
      'From: test@example.org\r\n' +
      'To: from@example.org\r\n' +
      `Subject: ${i}. MAIL\r\n` +
      '\r\n' +
      `MANY MAILS NUMBER ${i}`, [flag], new Date(now.getTime() - i * 60000));
  }
})();
