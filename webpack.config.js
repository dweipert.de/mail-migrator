const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader');

const isProduction = process.env.NODE_ENV === 'production';

// main config
const config = {
  mode: isProduction ? 'production' : 'development',
  devtool: isProduction ? false : 'source-map',

  entry: {
    index: './app/src/index.js',
  },

  output: {
    path: path.resolve(__dirname, 'app/build'),
    filename: '[name].js',
    clean: true,
  },

  resolve: {
    alias: {
      '~': path.join(__dirname, 'app/src'),
    },
    extensions: ['.js', '.vue'],
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },

      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env', {
              targets: {
                browsers: ['> 1%', 'last 2 versions'],
              },
            }],
          ],
          plugins: [
            '@babel/plugin-transform-runtime',
          ],
        },
        exclude: path.resolve(__dirname, 'node_modules'),
      },

      {
        test: /\.s?css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ],
      },
      {
        test: /\.(jpg|png)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          outputPath: 'assets/img',
          esModule: false,
        },
      },
    ],
  },

  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin(),

    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: true,
      __VUE_PROD_DEVTOOLS__: false,
    }),

    new webpack.ExternalsPlugin('commonjs', ['electron']),
  ],

  target: 'electron-renderer',
};

module.exports = config;
